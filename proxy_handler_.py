import os


class handler(object):
 	"""
 	activate/deactivate proxy using os environ
 	assuming you are using a common system-wide proxy
 	"""

 	def __init__(self):
 		self.status_='default-system-setting'
	
	def deactivate(self):
		os.environ['NO_PROXY']='*'
		self.status_='inactive'		

	def activate(self):
		if 'NO_PROXY' in os.environ:os.environ.pop('NO_PROXY')
		self.status_='default-system-setting'

	def status(self):
		return self.status_